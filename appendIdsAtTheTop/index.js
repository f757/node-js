const axios = require('axios');
const fs = require('fs');
const idsToAppend = ['-3444', '5656', '-79343'];
const outputFile = 'output.json';
(async () => {
    try {
        console.log('[LOG] Downloading file');
        const fetchedData = await axios({
            url: 'https://gitlab.com/f757/node-js/-/raw/main/appendIdsAtTheTop/file.json?inline=false',
            responseType: 'blob'
        });
        // we can use also readFileSync
        // const file = fs.readFileSync('./file.json');
        // const jsonFile = JSON.parse(file);

        const { data } = fetchedData;
        console.log('[LOG] Appending IDs to each array of original file');
        for (const array of data) {
            const { ids } = array;
            ids.unshift(idsToAppend[0], idsToAppend[1], idsToAppend[2]);
        }
        const completedFile = JSON.stringify(data);
        console.log('[LOG] Writing completed video offer chooser');
        fs.writeFileSync(outputFile, completedFile);
    } catch (error) {
        throw new Error(`[ERROR] ${error.message}`);
    }
})().catch(error => {
    console.log(error.message);
    process.exit(1);
});