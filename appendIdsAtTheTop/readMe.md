# appendIdsAtTheTop - Modify a .json file

It is custom script modifing the .json file.
The script append particular ids at the beginning of each array.

## Installation

Use the package manager [npm] to install fs, axios.

```bash
npm install axios fs
```

## Usage

```nodeJS
node index.js
