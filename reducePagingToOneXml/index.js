/**
 * the script paginates thru clients endpoint and downloads over 2000 feeds
 * then merges them into one feed XML
 * the feeds are not available from every network, client enabled it only for our servers
 * for the test environment I use passThru endpoint to access the feeds
 * It's configurable as an option in the command line
 */
const axios = require('axios');
const {
    writeFileSync
} = require('fs');
const {
    Command
} = require('commander');

const outputXml = 'done_feed.xml';
const program = new Command();
program
    .option('-up, --useproxy <type>', 'Use proxy');
program.parse(process.argv);
const programOptions = program.opts();
let feedUrl = `https://gitlab.com/f757/node-js/-/raw/main/reducePagingToOneXml/page=PAGENUMBER.xml?inline=false`;
if (programOptions.useproxy === 'true') {
    feedUrl = 'http://ezproxy.yourlib.org/' + encodeURIComponent(feedUrl);
}

(async () => {
    try {
        let counter = 1;
        console.log('[LOG] Start of processing');
        while (!!counter) {
            const downloadedPage = await axios({
                url: feedUrl.replace('PAGENUMBER', counter),
                responseType: 'blob'
            });
            console.log(downloadedPage);
            const {
                headers: {
                    'content-length': contentLength
                }
            } = downloadedPage;
            const {
                data
            } = downloadedPage;
            const xmlWithoutLastElement = data.split('</CATALOG>')[0];
            const xmlWithoutFirstElement = data.split('<CATALOG>')[1];
            const xmlWithoutFirstAndLastElement = xmlWithoutFirstElement.split('</CATALOG>')[0];
            if (contentLength > 1000) {
                if (counter === 1) {
                    writeFileSync(outputXml, xmlWithoutLastElement);
                    console.log(`[LOG] End of processing page number: ${counter}`);
                } else {
                    writeFileSync(outputXml, xmlWithoutFirstAndLastElement, {
                        flag: 'a'
                    });
                    console.log(`[LOG] End of processing page number: ${counter}`);
                }
                counter++;
            } else {
                console.log('[LOG] End of processing');
                writeFileSync(outputXml, '</CATALOG>', {
                    flag: 'a'
                });
                counter = null;
                break;
            }
        }
    } catch (error) {
        throw new Error(`[ERROR] ${error.message}`);
    }

})().catch(error => {
    console.log(error.message);
    process.exit(1);
});