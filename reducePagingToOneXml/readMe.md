# reducePagingToOneXml - Merging multiple XML files into one XML

the script paginates thru endpoint and downloads feed until content length is higher than 1000. Then it merges them into one XML.
For training purpose I added an option in a command line "--useproxy". We can add proxy tool by URL to passThru endpoint not available from every network.

XML source: https://www.w3schools.com/xml/cd_catalog.xml

## Installation

Use the package manager [npm] to install fs, axios, commander.

```bash
npm install axios fs commander
```

## Usage

```bash
with proxy:
node index.js --useproxy true 

without proxy:
node index.js

